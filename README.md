**Charlotte cat vet**

Our Charlotte NC vet offers a wide range of preventive care services to help your pet live a healthier, happier 
life and improve the likelihood of early detection of problems before they become serious and expensive. 
Based on the guidelines developed by the American Animal Hospital Association (AAHA) and the American Veterinary Medical Association (AVMA), 
our cat veterinarians in Charlotte NC make their annual preventive care 
recommendations and take into account the genetic factors, age, medical history and lifestyle of your cat.
Please Visit Our Website [Charlotte cat vet](https://vetsincharlottenc.com/cat-vet.php) for more information. 

---

## Our cat vet in Charlotte services

Our cat vets in Charlotte NC will take full medical history, make dietary recommendations, evaluate actions and examine any 
identified medical problems for at least one annual Preventive Care Exam for cats aged 1-6 and Bi-Annual Senior 
Exams for cats aged 7 and up at that time.

we offer :
Ear and Eye Examination 
Cardiopulmonary analyzes (Heart and Lung) 
Reading the temperature to read 
Palpation of the abdomen 
Dental examination 
Dermatological review 
Musculoskeletal assessment

Vaccination recommendations include Rabies Core and Feline Distemper Core vaccinations. 
The Feline Leukemia vaccine may also be recommended for outdoor cats by your pet vet in Charlotte NC.
